import random


def print_infor_dict_cases(dict):
    for k, v in dict.items():
        print("PARA " + str(k))

        for l_v in v:
            print(l_v)

        print(len(v))
        print("\n")


def get_dict_all_cases():
    dict_cases = {}

    file_cases = open("data.set/dermatology.data")
    file_cases_content = file_cases.read()

    list_cases = file_cases_content.split("\n")

    list_p = []  # 1
    list_sd = []  # 2
    list_lp = []  # 3
    list_pr = []  # 4
    list_cd = []  # 5
    list_ppr = []  # 6
    for c in list_cases:
        list_case = c.split(",")
        at_last_case = len(list_case) - 1
        last_item = list_case[at_last_case]  # Pegando o último item da linha

        if last_item == '1':
            list_p.append(list_case[0:at_last_case])  # Pegando a linha desconsiderando o último item
        elif last_item == '2':
            list_sd.append(list_case[0:at_last_case])  # Pegando a linha desconsiderando o último item
        elif last_item == '3':
            list_lp.append(list_case[0:at_last_case])  # Pegando a linha desconsiderando o último item
        elif last_item == '4':
            list_pr.append(list_case[0:at_last_case])  # Pegando a linha desconsiderando o último item
        elif last_item == '5':
            list_cd.append(list_case[0:at_last_case])  # Pegando a linha desconsiderando o último item
        elif last_item == '6':
            list_ppr.append(list_case[0:at_last_case])  # Pegando a linha desconsiderando o último item

    for i in range(1, 6):
        if i == 1:
            dict_cases.__setitem__(i, list_p)
        elif i == 2:
            dict_cases.__setitem__(i, list_sd)
        elif i == 3:
            dict_cases.__setitem__(i, list_lp)
        elif i == 4:
            dict_cases.__setitem__(i, list_pr)
        elif i == 5:
            dict_cases.__setitem__(i, list_cd)
        elif i == 6:
            dict_cases.__setitem__(i, list_ppr)

    return dict_cases


def convert_values_for_float(list_case):
    list_to_return = []
    for i in list_case:
        list_to_return.append(float(i))

    return list_to_return


def get_complete_list():
    file_cases = open("data.set/dermatology.data")
    file_cases_content = file_cases.read()

    list_cases = file_cases_content.split("\n")

    list_return = []

    for l in list_cases:
        list_case = l.split(",")
        list_case = list_case[0:len(list_case) - 1]  # Retirando o último

        if list_case[len(list_case) - 1] == '?':
            list_case.insert(len(list_case) - 1, 30)  # Substituindo ? por 30
            list_case = list_case[0:len(list_case) - 1]# Retirando o último que agora é ?

        list_case = convert_values_for_float(list_case)

        list_return.append(list_case)

    return list_return


def get_list_of_trainig(list_complete):
    # list_complete = list_complete.shuffle()

    random.shuffle(list_complete)

    list_to_return_training = list_complete[0:292]
    list_to_return_tests = list_complete[292:366]

    return list_to_return_training, list_to_return_tests


def get_list_of_weights():
    list_of_list_weights = []

    for i in range(0, 6):
        list_weights = []

        for i in range(0, 35):
            # number_weight = random.random()
            number_weight = random.random() * 0.5
            list_weights.append(number_weight)

        list_of_list_weights.append(list_weights)

    return list_of_list_weights


def print_weights(list_of_weightsfor_qi):
    for p in list_of_weightsfor_qi:
        print(len(p))
        print(p)
        # print("")

    print("")
    print(len(list_of_weightsfor_qi))


def generate_transpose(list_c):
    list_to_return = [[list_c[j][i] for j in range(len(list_c))] for i in range(len(list_c[0]))]

    return list_to_return


def get_list_normalized(list_complete):
    list_to_return = []

    transpose_list = generate_transpose(list_complete)

    for list_case in transpose_list:

        max_list = max(list_case)
        min_list = min(list_case)

        list_normalized = []

        for i in list_case:
            new_normalized = (2 * ((float(i) - float(min_list)) / (float(max_list) - float(min_list)))) - 1
            list_normalized.append(new_normalized)

        list_to_return.append(list_normalized)

    return generate_transpose(list_to_return)


def get_output_normalized(list_outs):
    list_to_return = []

    max_list = max(list_outs)
    min_list = min(list_outs)

    for list_o in list_outs:
        new_normalized = (2*(float(list_o) - float(min_list)) / (float(max_list) - float(min_list)))-1

        list_to_return.append(new_normalized)

    # list_to_return.reverse()

    return list_to_return


def calculate_y(sum):
    if sum >= 1:
        return 1
    return -1


def calculate_output(list_entradas, list_pesos):
    s = 0.0
    for i, j in zip(list_entradas, list_pesos):
        if i == -1:
            s += (float(i) * float(1))
        else:
            s += (float(i) * float(j))

    return calculate_y(s)
