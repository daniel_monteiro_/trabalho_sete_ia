# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import datetime


def generate_graphic_hits_ok(list_rounds, qtd_rounds):
    plt.plot(list_rounds)
    plt.ylabel("Desempenho de acertos da rede neural")
    plt.xlabel("Rodadas de execução")

    plt.savefig("graphics/" + str(qtd_rounds) + "_hits_ok_" + datetime.datetime.now().strftime("%d_%m_%Y__%H_%M_%S") + '.pdf')


def generate_graphic_hits_error(list_rounds, qtd_rounds):
    plt.plot(list_rounds)
    plt.ylabel("Erros da rede neural")
    plt.xlabel("Rodadas de execução")

    plt.savefig("graphics/" + str(qtd_rounds) + "_hits_error_" + datetime.datetime.now().strftime("%d_%m_%Y__%H_%M_%S") + '.pdf')
